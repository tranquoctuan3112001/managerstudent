package com.example.managerstudent.controller;

import com.example.managerstudent.entity.StudentEntity;
import com.example.managerstudent.service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



import java.util.List;
@RestController
@RequestMapping("/students")
public class TestController {
    @Autowired
    private StudentServiceImpl studentServiceImpl;
    @GetMapping("/v1")
    private ResponseEntity<List<StudentEntity>> getStudents(){
        List<StudentEntity> ls = studentServiceImpl.getAllStudent();
        return ResponseEntity.ok(ls);
    }

    @PostMapping("/v1")
    private ResponseEntity<List<StudentEntity>> saveStudent(@RequestBody StudentEntity st){
        studentServiceImpl.saveStudent(st);
        List<StudentEntity> ls = studentServiceImpl.getAllStudent();
        return new ResponseEntity<>(ls, HttpStatus.CREATED);
    }

    @PutMapping("/v1")
    private ResponseEntity<StudentEntity> editStudent(@RequestBody StudentEntity st){
        studentServiceImpl.saveStudent(st);
        return ResponseEntity.ok(st);
    }

    @DeleteMapping("/v1/{id}")
    private ResponseEntity<List<StudentEntity>> deleteStudent(@PathVariable("id") int id){
        StudentEntity st = studentServiceImpl.getStudentById(id);
        studentServiceImpl.deleteStudent(st);
        List<StudentEntity> ls = studentServiceImpl.getAllStudent();
        return ResponseEntity.ok(ls);
    }
}
