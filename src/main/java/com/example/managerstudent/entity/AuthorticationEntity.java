package com.example.managerstudent.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="authors")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AuthorticationEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int authorId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="studentId")
    private StudentEntity studentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="roleId")
    private RolesEntity roleId;
}
