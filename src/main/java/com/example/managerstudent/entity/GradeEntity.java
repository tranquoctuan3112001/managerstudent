package com.example.managerstudent.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="grades")
public class GradeEntity implements Serializable {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="studentId", nullable = false)
    private StudentEntity studentId;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="subjectId", nullable = false)
    private SubjectEntity subjectId;


}
