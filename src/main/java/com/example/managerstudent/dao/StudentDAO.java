package com.example.managerstudent.dao;

import com.example.managerstudent.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentDAO extends JpaRepository<StudentEntity , Integer> {
}
