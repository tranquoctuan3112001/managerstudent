package com.example.managerstudent.service;

import com.example.managerstudent.dao.StudentDAO;
import com.example.managerstudent.entity.StudentEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDAO studentDAO;
    @Override
    public List<StudentEntity> getAllStudent() {
        return studentDAO.findAll();
    }

    @Override
    public StudentEntity getStudentById(int id) {
        return studentDAO.getById(id);
    }

    @Override
    public void deleteStudent(StudentEntity st) {
        studentDAO.delete(st);
    }

    @Override
    public StudentEntity saveStudent(StudentEntity st) {
        studentDAO.save(st);
        return st;
    }
}
