package com.example.managerstudent.service;

import com.example.managerstudent.dao.StudentDAO;
import com.example.managerstudent.entity.StudentEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface StudentService  {
    List<StudentEntity> getAllStudent();
    StudentEntity getStudentById(int id);
    void deleteStudent(StudentEntity st);
    StudentEntity saveStudent(StudentEntity st);
}
